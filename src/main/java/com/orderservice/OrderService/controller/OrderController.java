package com.orderservice.OrderService.controller;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/create")
    public ResponseEntity<String> createOrder(@Valid @RequestBody OrderDTO data){
        return orderService.createOrder(data);
    }

    @PostMapping("/update")
    public ResponseEntity<String> updateOrder(@Valid @RequestBody OrderDTO data){
        return orderService.updateOrder(data);
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return orderService.getOrdersByUserId(id);
    }

    @GetMapping("/getAll")
    public List<OrderDTO> getAllOrders(){
        return orderService.getAllOrders();
    }
}
