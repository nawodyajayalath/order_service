package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.entities.OrderEntity;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;



@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    /**
     * ===========================================
     * Get all orders by using UserID
     * ==========================================
     * @param id
     * @return
     */
    public List<OrderDTO> getOrdersByUserId(long id){
        LOGGER.info("/========== ENTER INTO getOrderByUserId in UserService ===============/");
        List<OrderDTO> orders = null;
        try{
            orders = orderRepository.findOrdersByUserId(Long.toString(id))
                    .stream()
                    .map(orderEntity -> new OrderDTO(
                            orderEntity.getId(),
                            orderEntity.getOrderId(),
                            orderEntity.getUserId()
                    )).collect(Collectors.toList());
        } catch (Exception e){
            LOGGER.warn("Exception in getOrdersByUserId e: " + e);
        }
        return orders;
    }

    /**
     * ==================================
     *  Use to get all order
     *  ================================
     * @return as list of orderDTO
     */
    public List<OrderDTO> getAllOrders(){
        return orderRepository.findAll().stream().map(
                orderEntity -> new OrderDTO(
                        orderEntity.getId(),
                        orderEntity.getOrderId(),
                        orderEntity.getUserId().toString()
                )
        ).collect(Collectors.toList());
    }

    /***
     * ============================================
     * Method use to create an order
     * ============================================
     * @param order
     * @return response entity with success or server error
     */
    public ResponseEntity<String> createOrder(OrderDTO order){
        LOGGER.info("==================== Entered into createOrder method inside OrderService ============");
        try{
            OrderEntity orderEntity = new OrderEntity(order.getUserId(), order.getOrderId());
            orderRepository.save(orderEntity);

            LOGGER.info("Order created successfully : {}", orderEntity.getOrderId());
            return new ResponseEntity<>(
                    "Order created successfully!",
                    HttpStatus.CREATED
            );
        } catch (Exception e){
            LOGGER.error("Error creating order : " + e.getMessage());
            return new ResponseEntity<>(
                    "Someting went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /***
     * =====================================
     * Method to use update existing orders
     * =====================================
     * @param order orderDTO
     * @return ResponseEntity with success or error message
     */
    public ResponseEntity<String> updateOrder(OrderDTO order){
        LOGGER.info("================ Entered into updateOrder method in orderService ============");
        try{
            if(order.getId() == null)
                return new ResponseEntity<>("ID can't be empty!", HttpStatus.BAD_REQUEST);

            OrderEntity orderEntity = orderRepository.getById(order.getId());
            orderEntity.setOrderId(order.getOrderId());
            orderEntity.setUserId(order.getUserId());
            orderRepository.save(orderEntity);

            LOGGER.info("Order updated successfully: {}", orderEntity.getOrderId());

            return new ResponseEntity<>(
                    "Order updated successfully!",
                    HttpStatus.OK
            );
        } catch (Exception e){
            LOGGER.error("Error creating order : " + e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

}
